/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGEventHandler.h"

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\Convert.h>
#include <SDK\Contact.h>
#include <SDK\Message.h>

// Plugin includes
#include "GGPlugin.h"
#include "Helpers\GGError.h"
#include "Helpers\GGProtocolHelper.h"

// Libgadu includes
#include "lib\libgadu\libgadu_functions.h"

GGEventHandler::GGEventHandler(GGSession* Session)
{
	this->Session = Session;
	Synchronizer = new CSynchronizer();
}

GGEventHandler::~GGEventHandler()
{
	delete Synchronizer;
}

UnicodeString GGEventHandler::DumpConnectionState()
{
	switch (Session->Session()->state)
	{
	case GG_STATE_RESOLVING:
		return "Resolving address";
		break;
	case GG_STATE_CONNECTING_HUB:
		return "Connecting to hub";
		break;
	case GG_STATE_READING_DATA:
		return "Fetching data from hub";
		break;
	case GG_STATE_CONNECTING_GG:
		return "Connecting to server";
		break;
	case GG_STATE_READING_KEY:
		return "Waiting for hash key";
		break;
	case GG_STATE_READING_REPLY:
		return "Sending key";
		break;
	case GG_STATE_CONNECTED:
		return "Connected";
		break;
	case GG_STATE_IDLE:
		return "Idle!";
		break;
	case GG_STATE_ERROR:
		return "state==error! error=" + Session->Session()->error;
		break;
	default:
		return "unknown state! state=" + Session->Session()->state;
		break;
	}
}

void GGEventHandler::HandleEventNotify(struct gg_event* e)
{
	gg_notify_reply* Notify = (GG_EVENT_NOTIFY_DESCR == e->type) ?
		e->event.notify_descr.notify : e->event.notify;

	while (Notify->uin)
	{
		UnicodeString JID = IntToStr((int)Notify->uin) + "@gg.contact";
		CContact* Contact = GGPlugin::instance()->GetContactManager()
			->GetContactByJID(JID);

		if (Contact != NULL)
		{
			Contact->Status.State = GGProtocolHelper::StatusFromGGStatus
				(Notify->status);
			Contact->Status.Description = (GG_EVENT_NOTIFY_DESCR == e->type) ?
				e->event.notify_descr.descr : "";
			Synchronizer->UpdateContact(Contact);
		}
		Notify++;
	}

}

void GGEventHandler::HandleEventNotify60(struct gg_event* e)
{
	gg_event_notify60* Notify = e->event.notify60;

	while (Notify->uin)
	{
		UnicodeString JID = IntToStr((int)Notify->uin) + "@gg.contact";
		CContact* Contact = GGPlugin::instance()->GetContactManager()
			->GetContactByJID(JID);

		if (Contact != NULL)
		{
			Contact->Status.State = GGProtocolHelper::StatusFromGGStatus
				(Notify->status);
			Contact->Status.Description = Notify->descr;
			Synchronizer->UpdateContact(Contact);
		}
		Notify++;
	}
}

void GGEventHandler::HandleEventStatus(struct gg_event* e)
{
	if (GG_EVENT_STATUS60 == e->type)
	{
		UnicodeString JID = IntToStr((int)e->event.status60.uin) +
			"@gg.contact";
		CContact* Contact = GGPlugin::instance()->GetContactManager()
			->GetContactByJID(JID);

		if (Contact != NULL)
		{
			Contact->Status.State = GGProtocolHelper::StatusFromGGStatus
				(e->event.status60.status);
			Contact->Status.Description = e->event.status60.descr;
			Synchronizer->UpdateContact(Contact);
		}
	}
	else
	{
		UnicodeString JID = IntToStr((int)e->event.status.uin) + "@gg.contact";
		CContact* Contact = GGPlugin::instance()->GetContactManager()
			->GetContactByJID(JID);

		if (Contact != NULL)
		{
			Contact->Status.State = GGProtocolHelper::StatusFromGGStatus
				(e->event.status.status);
			Contact->Status.Description = e->event.status.descr;
			Synchronizer->UpdateContact(Contact);
		}
	}
}

void GGEventHandler::HandleEventConnFailed(struct gg_event* e)
{
	GG::GGError Error;

	switch (e->event.failure)
	{
	case GG_FAILURE_RESOLVING:
		Error = GG::ConnectionServerNotFound;
		break;
	case GG_FAILURE_CONNECTING:
		Error = GG::ConnectionCannotConnect;
		break;
	case GG_FAILURE_NEED_EMAIL:
		Error = GG::ConnectionNeedEmail;
		break;
	case GG_FAILURE_INVALID:
		Error = GG::ConnectionInvalidData;
		break;
	case GG_FAILURE_READING:
		Error = GG::ConnectionCannotRead;
		break;
	case GG_FAILURE_WRITING:
		Error = GG::ConnectionCannotWrite;
		break;
	case GG_FAILURE_PASSWORD:
		Error = GG::ConnectionIncorrectPassword;
		break;
	case GG_FAILURE_TLS:
		Error = GG::ConnectionTlsError;
		break;
	case GG_FAILURE_INTRUDER:
		Error = GG::ConnectionIntruderError;
		break;
	case GG_FAILURE_UNAVAILABLE:
		Error = GG::ConnectionUnavailableError;
		break;

	default:
		AQQ::Functions::Log(0,"ERROR: unhandled/unknown connection error! " +
			e->event.failure);
		Error = GG::ConnectionUnknow;
		break;
	}
}

void GGEventHandler::HandleEventConnSuccess(struct gg_event* e)
{

}

void GGEventHandler::HandleEventDisconnect(struct gg_event* e)
{
	Session->LogOff();
}

void GGEventHandler::HandleEventMultilogonInfo(struct gg_event* e)
{

}

void GGEventHandler::HandleEventMessage(struct gg_event* e)
{
	gg_event_msg Msg = (GG_EVENT_MSG == e->type) ? e->event.msg :
		e->event.multilogon_msg;

	if (Msg.sender != 0)
	{
		UnicodeString JID = IntToStr((int)Msg.sender) + "@gg.contact";
		CContact* Contact = GGPlugin::instance()->GetContactManager()
			->GetContactByJID(JID);

		CMessage* Message = new CMessage();
		Message->JID = JID;
		Message->Date = Msg.time;
		Message->ChatState = CHAT_NONE;
		Message->Body = CConvert::instance().ToUnicode(Msg.message);
		Message->Offline = false;
		Message->DefaultNick = Contact != NULL ? Contact->Nick :
			IntToStr((int)Msg.sender);
		Message->Store = true;
		Message->Kind = Msg.recipients_count == 0 ? MSGKIND_CHAT :
			MSGKIND_GROUPCHAT;
		Message->ShowAsOutgoing = false;
		Synchronizer->NotifyMessage(Message);
	}
}
