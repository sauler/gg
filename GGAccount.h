/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <vcl.h>

// SDK includes
#include <SDK\Account.h>

// Plugin includes
#include "GGAccountDetails.h"
#include "GGConnection.h"
#include "GGStateChange.h"
#include "GGStateChangeService.h"
#include "UI\AddAccountFrm.h"

class GGAccount: public CAccount
{
private:
	//GGAccountDetails* Details;

	GGStateChange* StateChange;
	GGStateChangeService* StateChangeService;

	GGConnection* Connection;

	TAddAccountForm* AddAccountForm;

	virtual void OnDefaultEvent() {}
	virtual void OnNewEvent() {}
	virtual void OnEditEvent();
	virtual void OnDeleteEvent();
	virtual void OnChangePassEvent();
public:
	GGAccount(UnicodeString Name, bool CreateStateButton);
	GGAccount(UnicodeString Name, UnicodeString Password);
	~GGAccount();
	GGAccountDetails* GetDetails();
	GGStateChange* GetStateButton();
};
