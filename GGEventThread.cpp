/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGEventThread.h"

// VCL includes
#include <System.hpp>
#include <vcl.h>

// SDK includes
#include <SDK\IconManager.h>

// Libgadu includes
#include "lib\libgadu\libgadu_functions.h"

// Plugin includes
#include "GGAccount.h"
#include "GGStateChange.h"

// Other includes
#include "time.h"

__fastcall GGEventThread::GGEventThread(bool CreateSuspended, GGConnection* Connection):
	 TThread(CreateSuspended)
{
	this->NameThreadForDebugging(UnicodeString("GGEventThread"),this->ThreadID);
	this->FreeOnTerminate = false;
	last = 0;
	this->Connection = Connection;
	this->Session = Connection->GetSession();

	EventHandler = new GGEventHandler(Session);
}

__fastcall GGEventThread::~GGEventThread()
{
	if (Session->Exists())
		Session->Free();

	if (Session->EventExists())
		Session->FreeEvent();

	if (EventHandler)
		delete EventHandler;
}

void __fastcall GGEventThread::Execute()
{
	for (;;)
	{
		FD_ZERO(&rd);
		FD_ZERO(&wd);

		if ((Session->Session()->check & GG_CHECK_READ))
			FD_SET(Session->Session()->fd, &rd);
		if ((Session->Session()->check & GG_CHECK_WRITE))
			FD_SET(Session->Session()->fd, &wd);

		tv.tv_sec = 1;
		tv.tv_usec = 0;

		Result = select(Session->Session()->fd + 1, &rd, &wd, NULL, &tv);

		now = time(NULL);

		if (now != last)
			if (Session->Session()->timeout != -1 && Session->Session()->timeout-- == 0 &&
				!Session->Session()->soft_timeout)
			{
				ShowMessage("Przekroczenie czasu operacji.");
				Session->Free();
				this->Terminate();
			}



	if (Session->Exists() && (FD_ISSET(Session->Session()->fd, &rd) || FD_ISSET(Session->Session()->fd, &wd) ||
		(Session->Session()->timeout == 0 && Session->Session()->soft_timeout)))
		{
			if (!Session->WatchFor())
			{
				ShowMessage("Polaczenie zerwane.");
				Session->Free();
				this->Terminate();
			}

			switch (Session->Event()->type)
			{
			case GG_EVENT_MSG:
			case GG_EVENT_MULTILOGON_MSG:
				EventHandler->HandleEventMessage(Session->Event());
				break;

			case GG_EVENT_MULTILOGON_INFO:
				// handleEventMultilogonInfo(e);
				break;

			case GG_EVENT_TYPING_NOTIFICATION:
				// typingNotificationEventReceived(e);
				break;

			case GG_EVENT_NOTIFY:
			case GG_EVENT_NOTIFY_DESCR:
				// handleEventNotify(e);
				break;

			case GG_EVENT_NOTIFY60:
				// handleEventNotify60(e);
				break;

			case GG_EVENT_STATUS:
			case GG_EVENT_STATUS60:
				// handleEventStatus(e);
				break;

			case GG_EVENT_ACK:
				// emit ackEventReceived(e);
				break;

			case GG_EVENT_CONN_FAILED:
			{
				ShowMessage("Failed to connect: " + IntToStr(Session->Event()->event.failure));
				Session->IsConnected(false);
				Connection->GetAccount()->GetStateButton()->StateButton->BlinkStopIcon =
					 CIconManager::instance()->GetIconByName("GG_OFFLINE");
				Connection->GetAccount()->GetStateButton()->StateButton->StopBlink();
				Session->Free();
				Session->FreeEvent();
				this->Terminate();
				return;
				break;
			}

			case GG_EVENT_CONN_SUCCESS:
			{
				// handleEventConnSuccess(e);
				//ShowMessage("Connected");
				Session->IsConnected(true);
				gg_notify(Session->Session(), NULL, 0);
				Connection->GetAccount()->GetStateButton()->StateButton->StopBlink();
			   //	Connection->GetAccount()->GetStateButton()->StateButton->IconID =
			   //		Connection->GetAccount()->GetStateButton()->StateButton->BlinkStopIcon;
			   //	Connection->GetAccount()->GetStateButton()->StateButton->Update();
				break;
			}

			case GG_EVENT_DISCONNECT:
				// handleEventDisconnect(e);
				Session->IsConnected(false);
				break;

			}
			//Session->FreeEvent();
		}

	}
}
