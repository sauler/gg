/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGConnection.h"

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\Convert.h>

// Plugin includes
#include "GGAccount.h"
#include "GGEventThread.h"
#include "Helpers\GGProtocolHelper.h"

// Libgadu includes
#include "lib\libgadu\libgadu_functions.h"

GGConnection::GGConnection(GGAccount* Account)
{
	this->Account = Account;
	Session = new GGSession();
	EventThread = new GGEventThread(true, this);
}

GGConnection::~GGConnection()
{
	if (Session)
		delete Session;
	if (EventThread)
	{
		EventThread->Terminate();
		delete EventThread;
	}
}

void GGConnection::SetupConnection()
{
	memset(&LoginParams, 0, sizeof(LoginParams));

	LoginParams.uin = StrToInt(Account->Name);
	LoginParams.password = CConvert::instance().ToChar(Account->GetDetails()->Password);
	LoginParams.async = 1;

	LoginParams.status = GG_STATUS_AVAIL_DESCR;
	LoginParams.status_descr = "test";
	LoginParams.tls = GG_SSL_ENABLED;

	LoginParams.client_version = CConvert::instance().ToChar("AQQ " + AQQ::System::AppVer());
	LoginParams.protocol_features =
		GG_FEATURE_ALL | GG_FEATURE_MULTILOGON;
}

GGAccount* GGConnection::GetAccount()
{
	return this->Account;
}

GGSession* GGConnection::GetSession()
{
	return this->Session;
}

bool GGConnection::Login()
{
	if (Session->Exists())
		Session->Free();

	SetupConnection();

	bool Result = Session->Login(&LoginParams);

	if (Result)
		EventThread->Start();

	return Result;
}

bool GGConnection::IsConnected()
{
	return Session->IsConnected();
}

void GGConnection::LogOff()
{
	if (Session->Exists())
	{
		gg_logoff(Session->Session());
		Session->Free();
		if (Session->EventExists())
			Session->FreeEvent();
	}
}

void GGConnection::SendStatus(int State, UnicodeString Description)
{
	if (!Session->Exists())
		return;

	CStatus Status;
	Status.State = State;
	Status.Description = Description;
	int Friends = Account->GetDetails()->PrivateStatus ? GG_STATUS_FRIENDS_MASK : 0;
	int Type = GGProtocolHelper::GGStatusFromStatus(Status);
	bool HasDescription = Status.HasDescription();

	SetStatusFlags();

	if (HasDescription)
		gg_change_status_descr(Session->Session(), Type | Friends, CConvert::instance().ToChar(Status.Description));
	else
		gg_change_status(Session->Session(), Type | Friends);
}

void GGConnection::SetStatusFlags()
{
	if (!Session->Exists())
		return;

	int StatusFlags = GG_STATUS_FLAG_UNKNOWN;
	if (Account->GetDetails() && !Account->GetDetails()->ReceiveSpam)
		StatusFlags = StatusFlags | GG_STATUS_FLAG_SPAM;

	gg_change_status_flags(Session->Session(), GG_STATUS_FLAG_UNKNOWN | StatusFlags);
}
