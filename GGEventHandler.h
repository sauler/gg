/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <vcl.h>

// SDK includes
#include <SDK\Synchronizer.h>

// Plugin includes
#include "GGSession.h"

class GGEventHandler
{
private:
	GGSession* Session;
	CSynchronizer* Synchronizer;
public:
	GGEventHandler(GGSession* Session);
	~GGEventHandler();

	UnicodeString DumpConnectionState();
	void HandleEventNotify(struct gg_event* e);
	void HandleEventNotify60(struct gg_event* e);
	void HandleEventStatus(struct gg_event* e);
	void HandleEventConnFailed(struct gg_event* e);
	void HandleEventConnSuccess(struct gg_event* e);
	void HandleEventDisconnect(struct gg_event* e);
	void HandleEventMultilogonInfo(struct gg_event* e);
	void HandleEventMessage(struct gg_event* e);
};
