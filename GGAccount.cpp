/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGAccount.h"

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\IconManager.h>

// Plugin includes
#include "GGPlugin.h"

GGAccount::GGAccount(UnicodeString Name, bool CreateStateButton)
{
	AddAccountForm = NULL;
	this->Name = Name;
	this->IconID = CIconManager::instance()->GetIconByName("GG_ONLINE");
	this->CanCreate = true;
	this->CanEdit = true;
	this->CanDelete = true;
	this->CanChangePass = true;
	this->DefaultEvent = true;
	this->Create();
	Details = new GGAccountDetails();


	Details->JID = Name + "@gg.contact";

	if (CreateStateButton)
	{
		Connection = new GGConnection(this);
		StateChange = new GGStateChange(GetAccountID(), this->Connection);
		StateChange->BuildStatePopup();
		StateChange->BuildStateButton();
		StateChangeService = new GGStateChangeService(StateChange);
	}
}

GGAccount::GGAccount(UnicodeString Name,UnicodeString Password)
{
	AddAccountForm = NULL;
	this->Name = Name;
	this->IconID = CIconManager::instance()->GetIconByName("GG_ONLINE");
	this->CanCreate = true;
	this->CanEdit = true;
	this->CanDelete = true;
	this->CanChangePass = true;
	this->DefaultEvent = true;
	this->Create();
	Details = new GGAccountDetails();

	Details->JID = Name + "@gg.contact";
	Details->Password = Password;

	Connection = new GGConnection(this);
	StateChange = new GGStateChange(GetAccountID(), this->Connection);
	StateChange->BuildStatePopup();
	StateChange->BuildStateButton();
	StateChangeService = new GGStateChangeService(StateChange);


	//Connection->Login();
}

GGAccount::~GGAccount()
{
	if (AddAccountForm)
		delete AddAccountForm;

	if (StateChangeService)
		delete StateChangeService;

	if (StateChange)
		delete StateChange;

	if (Details)
		delete Details;

	if (Connection)
  		delete Connection;
	this->Delete();
}

void GGAccount::OnEditEvent()
{
	if (!CanDelete)
	{
		if (GGPlugin::instance()->GetAccountManager()->GetAccountsCount() > 1)
		{
			AQQ::Functions::ShowMessage(1, "Wtyczka obs�uguje tylko jedno konto!");
			return;
		}

		if (!AddAccountForm)
			AddAccountForm = new TAddAccountForm(Application);
		AddAccountForm->Show();
	}
}

void GGAccount::OnDeleteEvent()
{
	GGPlugin::instance()->GetAccountManager()->GetAccountByID(GetAccountID())->EraseSettings();
	GGPlugin::instance()->GetAccountManager()->RemoveAccount(GetAccountID());
}

void GGAccount::OnChangePassEvent()
{
	AQQ::Functions::ShowMessage(0,"ChangePass");
}

GGAccountDetails* GGAccount::GetDetails()
{
	return (GGAccountDetails*)Details;
}

GGStateChange* GGAccount::GetStateButton()
{
	return StateChange;
}
