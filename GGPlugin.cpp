/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGPlugin.h"

// SDK includes
#include <SDK\Paths.h>
#include <SDK\IconManager.h>
#include <SDK\Theme.h>

// Plugin includes
#include "GGAccount.h"

GGPlugin* GGPlugin::Instance = 0;

GGPlugin::GGPlugin()
{
	CTheme::InitInstance();

	ExtractResources();
	LoadIcons();

	Protocol = new GGProtocol();

    AccountManager = new CAccountManager();
	AccountManager->AddAccount(new GGAccount("Dodaj konto GG", false));
	AccountManager->GetLastAddedAccount()->CanChangePass = false;
	AccountManager->GetLastAddedAccount()->CanDelete = false;
	AccountManager->GetLastAddedAccount()->Update();

	ContactManager = new CContactManager();

	AccountsSettings = new CSettings("Accounts.dat");
	LoadSettings();
}

GGPlugin::~GGPlugin()
{
	CTheme::ResetInstance();
	delete Protocol;
	delete AccountsSettings;
	delete ContactManager;
	delete AccountManager;
}

GGPlugin* GGPlugin::instance()
{
	return Instance;
}

void GGPlugin::InitInstance()
{
	Instance = new GGPlugin();
}

void GGPlugin::ResetInstance()
{
	delete Instance;
	Instance = NULL;
}

void GGPlugin::ExtractResources()
{
	UnicodeString IconsSaveDir;
	UnicodeString LibsSaveDir;
	UnicodeString SharedSaveDir;
	UnicodeString LanguagesSaveDir;

	IconsSaveDir = CPaths::instance()->PluginUserDir() + "\\GG\\Icons";
	LibsSaveDir = CPaths::instance()->PluginUserDir() + "\\GG\\Libs";
	SharedSaveDir = CPaths::instance()->PluginUserDir() + "\\Shared";
	LanguagesSaveDir = CPaths::instance()->PluginUserDir() + "\\Languages\\GG";

	if (!DirectoryExists(IconsSaveDir))
		AQQ::Paths::ForceDirectories(IconsSaveDir);

	if (!DirectoryExists(LibsSaveDir))
		AQQ::Paths::ForceDirectories(LibsSaveDir);

	if (!DirectoryExists(SharedSaveDir))
		AQQ::Paths::ForceDirectories(SharedSaveDir);

	// EXTRACT LANGUAGES
	if (!DirectoryExists(LanguagesSaveDir + "\\PL"))
		AQQ::Paths::ForceDirectories(LanguagesSaveDir + "\\PL");

	if (!DirectoryExists(LanguagesSaveDir + "\\EN"))
		AQQ::Paths::ForceDirectories(LanguagesSaveDir + "\\EN");

	// EXTRACT ICONS
	if (!FileExists(IconsSaveDir + "\\Online.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\Online.png", "ICON_ONLINE", "DATA");

	if (!FileExists(IconsSaveDir + "\\FFC.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\FFC.png", "ICON_FFC", "DATA");

	if (!FileExists(IconsSaveDir + "\\Away.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\Away.png", "ICON_AWAY", "DATA");

	if (!FileExists(IconsSaveDir + "\\DND.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\DND.png", "ICON_DND", "DATA");

	if (!FileExists(IconsSaveDir + "\\Invisible.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\Invisible.png", "ICON_INVISIBLE", "DATA");

	if (!FileExists(IconsSaveDir + "\\Offline.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\Offline.png", "ICON_OFFLINE", "DATA");

	if (!FileExists(IconsSaveDir + "\\Blocked.png"))
		AQQ::Functions::ExtractResource(IconsSaveDir + "\\Blocked.png", "ICON_BLOCKED", "DATA");

	if (!FileExists(SharedSaveDir + "\\GG.dll.png"))
		AQQ::Functions::ExtractResource(SharedSaveDir + "\\GG.dll.png", "ICON_SHARED", "DATA");

	// EXTRACT LIBS
	if (!FileExists(LibsSaveDir + "\\libffi-6.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libffi-6.dll", "LIB_FFI", "DATA");

	if (!FileExists(LibsSaveDir + "\\libgadu.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libgadu.dll", "LIB_GADU", "DATA");

	if (!FileExists(LibsSaveDir + "\\libgcc_s_sjlj-1.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libgcc_s_sjlj-1.dll", "LIB_GCC", "DATA");

	if (!FileExists(LibsSaveDir + "\\libgmp-10.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libgmp-10.dll", "LIB_GMP", "DATA");

	if (!FileExists(LibsSaveDir + "\\libgnutls-28.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libgnutls-28.dll", "LIB_GNUTLS", "DATA");

	if (!FileExists(LibsSaveDir + "\\libhogweed-2-4.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libhogweed-2-4.dll", "LIB_HOGWEED", "DATA");

	if (!FileExists(LibsSaveDir + "\\libintl-8.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libintl-8.dll", "LIB_INTL", "DATA");

	if (!FileExists(LibsSaveDir + "\\libnettle-4-6.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libnettle-4-6.dll", "LIB_NETTLE", "DATA");

	if (!FileExists(LibsSaveDir + "\\libp11-kit-0.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libp11-kit-0.dll", "LIB_P11", "DATA");

	if (!FileExists(LibsSaveDir + "\\libtasn1-6.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libtasn1-6.dll", "LIB_TASN", "DATA");

	if (!FileExists(LibsSaveDir + "\\libwinpthread-1.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\libwinpthread-1.dll", "LIB_WINPTHREAD", "DATA");

	if (!FileExists(LibsSaveDir + "\\zlib1.dll"))
		AQQ::Functions::ExtractResource(LibsSaveDir + "\\zlib1.dll", "LIB_ZLIB", "DATA");
}

void GGPlugin::LoadIcons()
{
	UnicodeString IconsDir;
	IconsDir = CPaths::instance()->PluginUserDir() + "\\GG\\Icons";

	CIconManager::instance()->LoadIcon("GG_ONLINE", IconsDir + "\\\\Online.png");
	CIconManager::instance()->LoadIcon("GG_FFC", IconsDir + "\\\\FFC.png");
	CIconManager::instance()->LoadIcon("GG_AWAY", IconsDir + "\\\\Away.png");
	CIconManager::instance()->LoadIcon("GG_DND", IconsDir + "\\\\DND.png");
	CIconManager::instance()->LoadIcon("GG_INVISIBLE", IconsDir + "\\\\Invisible.png");
	CIconManager::instance()->LoadIcon("GG_OFFLINE", IconsDir + "\\\\Offline.png");
	CIconManager::instance()->LoadIcon("GG_BLOCKED", IconsDir + "\\\\Blocked.png");
}

void GGPlugin::LoadSettings()
{
	TStringList* Sections;

	// Accounts
	Sections = new TStringList();
	AccountsSettings->ReadSections(Sections);

	for (int i = 0; i < Sections->Count; i++)
	{
		AccountManager->AddAccount(new GGAccount(Sections->Strings[i], true));
		GGAccount* Account = (GGAccount*)AccountManager->GetLastAddedAccount();
		Account->LoadSettings();
	}

	delete Sections;
}

CAccountManager* GGPlugin::GetAccountManager()
{
    return this->AccountManager;
}

CContactManager* GGPlugin::GetContactManager()
{
	return this->ContactManager;
}

GGProtocol* GGPlugin::GetProtocol()
{
	return this->Protocol;
}
