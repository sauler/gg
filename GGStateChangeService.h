/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <vcl.h>
#include <windows.h>

// SDK includes
#include <SDK\PluginAPI.h>

// Plugin includes
#include "GGStateChange.h"

class GGStateChangeService
{
public:
	GGStateChange* StateChange;

	HANDLE RegisterService(UnicodeString name,TAQQService serviceProc);
	INT_PTR UnregisterService(HANDLE hService);

	static GGStateChangeService* Instance;

	static INT_PTR __stdcall Online(WPARAM wParam,LPARAM lParam);
	static INT_PTR __stdcall FFC(WPARAM wParam,LPARAM lParam);
	static INT_PTR __stdcall Away(WPARAM wParam,LPARAM lParam);
	static INT_PTR __stdcall DND(WPARAM wParam,LPARAM lParam);
	static INT_PTR __stdcall Invisible(WPARAM wParam,LPARAM lParam);
	static INT_PTR __stdcall Offline(WPARAM wParam,LPARAM lParam);
	static INT_PTR __stdcall ChangeDesc(WPARAM wParam,LPARAM lParam);

public:
	GGStateChangeService(GGStateChange* StateChange);
	~GGStateChangeService();

	void CreateServices();
	void DestroyServices();
};

