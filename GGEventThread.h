/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <System.Classes.hpp>

// Libgadu includes
#include "lib\libgadu\libgadu.h"

// Plugin includes
#include "GGEventHandler.h"
#include "GGConnection.h"
#include "GGSession.h"

class GGEventThread: public TThread
{
private:
	struct timeval tv;
	fd_set rd, wd;
	time_t last, now;
	int Result;
	int count;
	GGEventHandler* EventHandler;
	GGConnection* Connection;
	GGSession* Session;
protected:
	void __fastcall Execute();
public:
	__fastcall GGEventThread(bool CreateSuspended, GGConnection* Connection);
	__fastcall ~GGEventThread();
};




