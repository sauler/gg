/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// Libgadu includes
#include "lib\libgadu\libgadu.h"

// Plugin includes
#include "GGSession.h"

class GGAccount;
class GGEventThread;

class GGConnection
{
private:
	struct gg_login_params LoginParams;
	GGAccount* Account;
	GGSession* Session;
	GGEventThread* EventThread;

	void SetupConnection();
public:
	GGConnection(GGAccount* Account);
	~GGConnection();

	GGAccount* GetAccount();
	GGSession* GetSession();
	bool Login();
	bool IsConnected();
	void LogOff();
	void SendStatus(int State, UnicodeString Description);
	void SetStatusFlags();
};
