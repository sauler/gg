/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGStateChange.h"

// SDK includes
#include <SDK\IconManager.h>

GGStateChange::GGStateChange(GGConnection* Connection)
{
	StateButton = new CStateButton();
	StatePopup = new CPopup();
	this->Connection = Connection;
	StateButton->BlinkStartIcon = CIconManager::instance()->GetIconByName("GG_OFFLINE");
}

GGStateChange::GGStateChange(int AccountID, GGConnection* Connection)
{
	StateButton = new CStateButton();
	StatePopup = new CPopup();
	this->AccountID = AccountID;
	this->Connection = Connection;
	StateButton->BlinkStartIcon = CIconManager::instance()->GetIconByName("GG_OFFLINE");
}

GGStateChange::~GGStateChange()
{
	if (StateButton)
		delete StateButton;
	if (StatePopup)
		delete StatePopup;
}

UnicodeString GGStateChange::GetPopupName()
{
	return "GGStatePopup" + IntToStr(this->AccountID);
}

UnicodeString GGStateChange::GetPopupItemName(UnicodeString State)
{
	return "GGState" + State + IntToStr(this->AccountID);
}

UnicodeString GGStateChange::GetPopupServiceName(UnicodeString State)
{
	return "GGState" + State + "Service" + IntToStr(this->AccountID);
}

UnicodeString GGStateChange::GetButtonName()
{
	return "GGStateButton" + IntToStr(this->AccountID);
}

void GGStateChange::BuildStatePopup()
{
	StatePopup->Name = GetPopupName();
	StatePopup->Create();

	// Online
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("Online"), "Dost�pny", 0,
		CIconManager::instance()->GetIconByName("GG_ONLINE"), GetPopupServiceName("Online")));

	// FFC
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("FFC"), "Pogadam", 1,
		CIconManager::instance()->GetIconByName("GG_FFC"), GetPopupServiceName("FFC")));

	// Separator 1
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("L1"), "-", 2, -1, ""));
	StatePopup->GetLastAddedItem()->Enabled = false;
	StatePopup->GetLastAddedItem()->Update();

	// Away
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("Away"), "Zaraz wracam", 3,
		CIconManager::instance()->GetIconByName("GG_AWAY"), GetPopupServiceName("Away")));

	// DND
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("DND"), "Nie przeszkadza�", 4,
		CIconManager::instance()->GetIconByName("GG_DND"), GetPopupServiceName("DND")));

	// Separator 2
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("L2"), "-", 5, -1, ""));
	StatePopup->GetLastAddedItem()->Enabled = false;
	StatePopup->GetLastAddedItem()->Update();

	// Invisible
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("Invisible"), "Niewidoczny", 6,
		CIconManager::instance()->GetIconByName("GG_INVISIBLE"), GetPopupServiceName("Invisible")));

	// Offline
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("Offline"), "Niedost�pny", 7,
		CIconManager::instance()->GetIconByName("GG_OFFLINE"), GetPopupServiceName("Offline")));


	// Separator 3
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("L3"), "-", 8, -1, ""));
	StatePopup->GetLastAddedItem()->Enabled = false;
	StatePopup->GetLastAddedItem()->Update();

	// Change description
	StatePopup->AddItem(new CPopupMenuItem(GetPopupItemName("ChangeDesc"), "Zmie� opis", 9,
		-1, GetPopupServiceName("ChangeDesc")));
}

void GGStateChange::BuildStateButton()
{
	StateButton->BlinkStartIcon = CIconManager::instance()->GetIconByName
		("GG_OFFLINE");
	StateButton->BlinkStopIcon = CIconManager::instance()->GetIconByName
		("GG_ONLINE");
	StateButton->CreateButton(GetButtonName(), 0,
		CIconManager::instance()->GetIconByName("GG_OFFLINE"), "",
		GetPopupName());
}

GGConnection* GGStateChange::GetConnection()
{
	return this->Connection;
}
