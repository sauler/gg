/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGProtocolHelper.h"

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\PluginAPI.h>

// Libgadu includes
 #include "lib\libgadu\libgadu.h"

// Defines
#define GG_STATUS_INVISIBLE2 0x0009
#define GG8_DESCRIPTION_MASK 0x00ff

unsigned int GGProtocolHelper::GGStatusFromStatus(const CStatus &Status)
{
	bool HasDescription = Status.HasDescription();

	if (CONTACT_FFC == Status.State)
		return HasDescription ? GG_STATUS_FFC_DESCR : GG_STATUS_FFC;

	if (CONTACT_ONLINE == Status.State)
		return HasDescription ? GG_STATUS_AVAIL_DESCR : GG_STATUS_AVAIL;

	if (CONTACT_AWAY == Status.State || CONTACT_NA == Status.State)
		return HasDescription ? GG_STATUS_BUSY_DESCR : GG_STATUS_BUSY;

	if (CONTACT_DND == Status.State)
		return HasDescription ? GG_STATUS_DND_DESCR : GG_STATUS_DND;

	if (CONTACT_INV == Status.State)
		return HasDescription ? GG_STATUS_INVISIBLE_DESCR : GG_STATUS_INVISIBLE;

	return HasDescription ? GG_STATUS_NOT_AVAIL_DESCR : GG_STATUS_NOT_AVAIL;
}

int GGProtocolHelper::StatusFromGGStatus(unsigned int Index)
{
	switch (Index & GG8_DESCRIPTION_MASK)
	{
		case GG_STATUS_FFC_DESCR:
		case GG_STATUS_FFC:
			return CONTACT_FFC;

		case GG_STATUS_AVAIL_DESCR:
		case GG_STATUS_AVAIL:
			return CONTACT_ONLINE;

		case GG_STATUS_BUSY_DESCR:
		case GG_STATUS_BUSY:
			return CONTACT_AWAY;

		case GG_STATUS_DND_DESCR:
		case GG_STATUS_DND:
			return CONTACT_DND;

		case GG_STATUS_INVISIBLE_DESCR:
		case GG_STATUS_INVISIBLE:
		case GG_STATUS_INVISIBLE2:
			return CONTACT_INV;

		case GG_STATUS_BLOCKED:
			return CONTACT_NULL;

		case GG_STATUS_NOT_AVAIL_DESCR:
		case GG_STATUS_NOT_AVAIL:

		default:
			return CONTACT_OFFLINE;
	}
}

UnicodeString GGProtocolHelper::ConnectionErrorMessage(GG::GGError Error)
{
	switch (Error)
	{
		case GG::ConnectionServerNotFound:
			return "Unable to connect, server has not been found";
		case GG::ConnectionCannotConnect:
			return "Unable to connect";
		case GG::ConnectionNeedEmail:
			return "Please change your email in \"Change password / email\" window. Leave new password field blank.";
		case GG::ConnectionInvalidData:
			return "Unable to connect, server has returned unknown data";
		case GG::ConnectionCannotRead:
			return "Unable to connect, connection break during reading";
		case GG::ConnectionCannotWrite:
			return "Unable to connect, connection break during writing";
		case GG::ConnectionIncorrectPassword:
			return "Unable to connect, invalid password";
		case GG::ConnectionTlsError:
			return "Unable to connect, error of negotiation TLS";
		case GG::ConnectionIntruderError:
			return "Too many connection attempts with bad password!";
		case GG::ConnectionUnavailableError:
			return "Unable to connect, servers are down";
		case GG::ConnectionUnknow:
			return "Connection broken";
		case GG::ConnectionTimeout:
			return "Connection timeout!";
		case GG::Disconnected:
			return "Disconnection has occurred";
		default:
			AQQ::Functions::Log(0, "Unhandled error? " + IntToStr(Error));
			return "Connection broken";
	}
}

bool GGProtocolHelper::IsConnectionErrorFatal(GG::GGError Error)
{
	switch (Error)
	{
		case GG::ConnectionNeedEmail:
		case GG::ConnectionIncorrectPassword:
		case GG::ConnectionIntruderError:
			return true;
		default:
			return false;
	}
}
