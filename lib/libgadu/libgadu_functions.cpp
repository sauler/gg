﻿/*******************************************************************************
 *                       Copyright (C) 2016 Rafał Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "libgadu_functions.h"
#include <windows.h>

bool LoadLibgadu(UnicodeString path) {
	handle = LoadLibraryEx(path.w_str(), NULL, LOAD_WITH_ALTERED_SEARCH_PATH);

	if (!handle) {
		int error = GetLastError();
		ShowMessage("ERROR: " + error);

	}

	gg_add_notify = (gg_add_notify_d)GetProcAddress(handle, "gg_add_notify");
	gg_add_notify_ex = (gg_add_notify_ex_d)GetProcAddress(handle, "gg_add_notify_ex");
	gg_change_passwd4 = (gg_change_passwd4_d)GetProcAddress(handle, "gg_change_passwd4");
	gg_change_status = (gg_change_status_d)GetProcAddress(handle, "gg_change_status");
	gg_change_status_descr = (gg_change_status_descr_d)GetProcAddress(handle, "gg_change_status_descr");
	gg_change_status_descr_time = (gg_change_status_descr_time_d)GetProcAddress(handle, "gg_change_status_descr_time");
	gg_change_status_flags = (gg_change_status_flags_d)GetProcAddress(handle, "gg_change_status_flags");
	gg_chat_create = (gg_chat_create_d)GetProcAddress(handle, "gg_chat_create");
	gg_chat_invite = (gg_chat_invite_d)GetProcAddress(handle, "gg_chat_invite");
	gg_chat_leave = (gg_chat_leave_d)GetProcAddress(handle, "gg_chat_leave");
	gg_chat_send_message = (gg_chat_send_message_d)GetProcAddress(handle, "gg_chat_send_message");
	gg_crc32 = (gg_crc32_d)GetProcAddress(handle, "gg_crc32");
	gg_dcc7_accept = (gg_dcc7_accept_d)GetProcAddress(handle, "gg_dcc7_accept");
	gg_dcc7_free = (gg_dcc7_free_d)GetProcAddress(handle, "gg_dcc7_free");
	gg_dcc7_reject = (gg_dcc7_reject_d)GetProcAddress(handle, "gg_dcc7_reject");
	gg_dcc7_send_file = (gg_dcc7_send_file_d)GetProcAddress(handle, "gg_dcc7_send_file");
	gg_dcc7_send_file_fd = (gg_dcc7_send_file_fd_d)GetProcAddress(handle, "gg_dcc7_send_file_fd");
	gg_dcc7_watch_fd = (gg_dcc7_watch_fd_d)GetProcAddress(handle, "gg_dcc7_watch_fd");
	gg_dcc_fill_file_info = (gg_dcc_fill_file_info_d)GetProcAddress(handle, "gg_dcc_fill_file_info");
	gg_dcc_fill_file_info2 = (gg_dcc_fill_file_info2_d)GetProcAddress(handle, "gg_dcc_fill_file_info2");
	gg_dcc_free = (gg_dcc_free_d)GetProcAddress(handle, "gg_dcc_free");
	gg_dcc_get_file = (gg_dcc_get_file_d)GetProcAddress(handle, "gg_dcc_get_file");
	gg_dcc_request = (gg_dcc_request_d)GetProcAddress(handle, "gg_dcc_request");
	gg_dcc_send_file = (gg_dcc_send_file_d)GetProcAddress(handle, "gg_dcc_send_file");
	gg_dcc_set_type = (gg_dcc_set_type_d)GetProcAddress(handle, "gg_dcc_set_type");
	gg_dcc_socket_create = (gg_dcc_socket_create_d)GetProcAddress(handle, "gg_dcc_socket_create");
	gg_dcc_voice_chat = (gg_dcc_voice_chat_d)GetProcAddress(handle, "gg_dcc_voice_chat");
	gg_dcc_voice_send = (gg_dcc_voice_send_d)GetProcAddress(handle, "gg_dcc_voice_send");
	gg_dcc_watch_fd = (gg_dcc_watch_fd_d)GetProcAddress(handle, "gg_dcc_watch_fd");
	gg_event_free = (gg_event_free_d)GetProcAddress(handle, "gg_event_free");
	gg_free_session = (gg_free_session_d)GetProcAddress(handle, "gg_free_session");
	gg_global_get_resolver = (gg_global_get_resolver_d)GetProcAddress(handle, "gg_global_get_resolver");
	gg_global_set_custom_resolver = (gg_global_set_custom_resolver_d)GetProcAddress(handle, "gg_global_set_custom_resolver");
	gg_global_set_resolver = (gg_global_set_resolver_d)GetProcAddress(handle, "gg_global_set_resolver");
	gg_http_connect = (gg_http_connect_d)GetProcAddress(handle, "gg_http_connect");
	gg_http_free = (gg_http_free_d)GetProcAddress(handle, "gg_http_free");
	gg_http_get_resolver = (gg_http_get_resolver_d)GetProcAddress(handle, "gg_http_get_resolver");
	gg_http_set_custom_resolver = (gg_http_set_custom_resolver_d)GetProcAddress(handle, "gg_http_set_custom_resolver");
	gg_http_set_resolver = (gg_http_set_resolver_d)GetProcAddress(handle, "gg_http_set_resolver");
	gg_http_stop = (gg_http_stop_d)GetProcAddress(handle, "gg_http_stop");
	gg_http_watch_fd = (gg_http_watch_fd_d)GetProcAddress(handle, "gg_http_watch_fd");
	gg_image_reply = (gg_image_reply_d)GetProcAddress(handle, "gg_image_reply");
	gg_image_request = (gg_image_request_d)GetProcAddress(handle, "gg_image_request");
	gg_is_gpl_compliant = (gg_is_gpl_compliant_d)GetProcAddress(handle, "gg_is_gpl_compliant");
	gg_login = (gg_login_d)GetProcAddress(handle, "gg_login");
	gg_logoff = (gg_logoff_d)GetProcAddress(handle, "gg_logoff");
	gg_multilogon_disconnect = (gg_multilogon_disconnect_d)GetProcAddress(handle, "gg_multilogon_disconnect");
	gg_notify = (gg_notify_d)GetProcAddress(handle, "gg_notify");
	gg_notify_ex = (gg_notify_ex_d)GetProcAddress(handle, "gg_notify_ex");
	gg_ping = (gg_ping_d)GetProcAddress(handle, "gg_ping");
	gg_pubdir50 = (gg_pubdir50_d)GetProcAddress(handle, "gg_pubdir50");
	gg_pubdir50_add = (gg_pubdir50_add_d)GetProcAddress(handle, "gg_pubdir50_add");
	gg_pubdir50_count = (gg_pubdir50_count_d)GetProcAddress(handle, "gg_pubdir50_count");
	gg_pubdir50_free = (gg_pubdir50_free_d)GetProcAddress(handle, "gg_pubdir50_free");
	gg_pubdir50_get = (gg_pubdir50_get_d)GetProcAddress(handle, "gg_pubdir50_get");
	gg_pubdir50_new = (gg_pubdir50_new_d)GetProcAddress(handle, "gg_pubdir50_new");
	gg_pubdir50_next = (gg_pubdir50_next_d)GetProcAddress(handle, "gg_pubdir50_next");
	gg_pubdir50_seq = (gg_pubdir50_seq_d)GetProcAddress(handle, "gg_pubdir50_seq");
	gg_pubdir50_seq_set = (gg_pubdir50_seq_set_d)GetProcAddress(handle, "gg_pubdir50_seq_set");
	gg_pubdir50_type = (gg_pubdir50_type_d)GetProcAddress(handle, "gg_pubdir50_type");
	gg_register3 = (gg_register3_d)GetProcAddress(handle, "gg_register3");
	gg_remind_passwd3 = (gg_remind_passwd3_d)GetProcAddress(handle, "gg_remind_passwd3");
	gg_remove_notify = (gg_remove_notify_d)GetProcAddress(handle, "gg_remove_notify");
	gg_remove_notify_ex = (gg_remove_notify_ex_d)GetProcAddress(handle, "gg_remove_notify_ex");
	gg_send_message = (gg_send_message_d)GetProcAddress(handle, "gg_send_message");
	gg_send_message_confer = (gg_send_message_confer_d)GetProcAddress(handle, "gg_send_message_confer");
	gg_send_message_confer_html = (gg_send_message_confer_html_d)GetProcAddress(handle, "gg_send_message_confer_html");
	gg_send_message_confer_richtext = (gg_send_message_confer_richtext_d)GetProcAddress(handle, "gg_send_message_confer_richtext");
	gg_send_message_ctcp = (gg_send_message_ctcp_d)GetProcAddress(handle, "gg_send_message_ctcp");
	gg_send_message_html = (gg_send_message_html_d)GetProcAddress(handle, "gg_send_message_html");
	gg_send_message_richtext = (gg_send_message_richtext_d)GetProcAddress(handle, "gg_send_message_richtext");
	gg_session_get_resolver = (gg_session_get_resolver_d)GetProcAddress(handle, "gg_session_get_resolver");
	gg_session_set_custom_resolver = (gg_session_set_custom_resolver_d)GetProcAddress(handle, "gg_session_set_custom_resolver");
	gg_session_set_resolver = (gg_session_set_resolver_d)GetProcAddress(handle, "gg_session_set_resolver");
	gg_token = (gg_token_d)GetProcAddress(handle, "gg_token");
	gg_token_free = (gg_token_free_d)GetProcAddress(handle, "gg_token_free");
	gg_token_watch_fd = (gg_token_watch_fd_d)GetProcAddress(handle, "gg_token_watch_fd");
	gg_typing_notification = (gg_typing_notification_d)GetProcAddress(handle, "gg_typing_notification");
	gg_unregister3 = (gg_unregister3_d)GetProcAddress(handle, "gg_unregister3");
	gg_userlist100_request = (gg_userlist100_request_d)GetProcAddress(handle, "gg_userlist100_request");
	gg_userlist_request = (gg_userlist_request_d)GetProcAddress(handle, "gg_userlist_request");
	gg_watch_fd = (gg_watch_fd_d)GetProcAddress(handle, "gg_watch_fd");
}

void FreeLibgadu() {
    FreeLibrary(handle);
}


// Libgadu handle
HINSTANCE handle;

// Libgadu functions
gg_add_notify_d gg_add_notify;
gg_add_notify_ex_d gg_add_notify_ex;

gg_change_passwd4_d gg_change_passwd4;
gg_change_status_d gg_change_status;
gg_change_status_descr_d gg_change_status_descr;
gg_change_status_descr_time_d gg_change_status_descr_time;
gg_change_status_flags_d gg_change_status_flags;
gg_chat_create_d gg_chat_create;
gg_chat_invite_d gg_chat_invite;
gg_chat_leave_d gg_chat_leave;
gg_chat_send_message_d gg_chat_send_message;
gg_crc32_d gg_crc32;

gg_dcc7_accept_d gg_dcc7_accept;
gg_dcc7_free_d gg_dcc7_free;
gg_dcc7_reject_d gg_dcc7_reject;
gg_dcc7_send_file_d gg_dcc7_send_file;
gg_dcc7_send_file_fd_d gg_dcc7_send_file_fd;
gg_dcc7_watch_fd_d gg_dcc7_watch_fd;
gg_dcc_fill_file_info_d gg_dcc_fill_file_info;
gg_dcc_fill_file_info2_d gg_dcc_fill_file_info2;
gg_dcc_free_d gg_dcc_free;
gg_dcc_get_file_d gg_dcc_get_file;
gg_dcc_request_d gg_dcc_request;
gg_dcc_send_file_d gg_dcc_send_file;
gg_dcc_set_type_d gg_dcc_set_type;
gg_dcc_socket_create_d gg_dcc_socket_create;
gg_dcc_voice_chat_d gg_dcc_voice_chat;
gg_dcc_voice_send_d gg_dcc_voice_send;
gg_dcc_watch_fd_d gg_dcc_watch_fd;

gg_event_free_d gg_event_free;

gg_free_session_d gg_free_session;

gg_global_get_resolver_d gg_global_get_resolver;
gg_global_set_custom_resolver_d gg_global_set_custom_resolver;
gg_global_set_resolver_d gg_global_set_resolver;

gg_http_connect_d gg_http_connect;
gg_http_free_d gg_http_free;
gg_http_get_resolver_d gg_http_get_resolver;
gg_http_set_custom_resolver_d gg_http_set_custom_resolver;
gg_http_set_resolver_d gg_http_set_resolver;
gg_http_stop_d gg_http_stop;
gg_http_watch_fd_d gg_http_watch_fd;

gg_image_reply_d gg_image_reply;
gg_image_request_d gg_image_request;
gg_is_gpl_compliant_d gg_is_gpl_compliant;

gg_login_d gg_login;
gg_logoff_d gg_logoff;

gg_multilogon_disconnect_d gg_multilogon_disconnect;

gg_notify_d gg_notify;
gg_notify_ex_d gg_notify_ex;

gg_ping_d gg_ping;
gg_pubdir50_d gg_pubdir50;
gg_pubdir50_add_d gg_pubdir50_add;
gg_pubdir50_count_d gg_pubdir50_count;
gg_pubdir50_free_d gg_pubdir50_free;
gg_pubdir50_get_d gg_pubdir50_get;
gg_pubdir50_new_d gg_pubdir50_new;
gg_pubdir50_next_d gg_pubdir50_next;
gg_pubdir50_seq_d gg_pubdir50_seq;
gg_pubdir50_seq_set_d gg_pubdir50_seq_set;
gg_pubdir50_type_d gg_pubdir50_type;

gg_register3_d gg_register3;
gg_remind_passwd3_d gg_remind_passwd3;
gg_remove_notify_d gg_remove_notify;
gg_remove_notify_ex_d gg_remove_notify_ex;

gg_send_message_d gg_send_message;
gg_send_message_confer_d gg_send_message_confer;
gg_send_message_confer_html_d gg_send_message_confer_html;
gg_send_message_confer_richtext_d gg_send_message_confer_richtext;
gg_send_message_ctcp_d gg_send_message_ctcp;
gg_send_message_html_d gg_send_message_html;
gg_send_message_richtext_d gg_send_message_richtext;
gg_session_get_resolver_d gg_session_get_resolver;
gg_session_set_custom_resolver_d gg_session_set_custom_resolver;
gg_session_set_resolver_d gg_session_set_resolver;

gg_token_d gg_token;
gg_token_free_d gg_token_free;
gg_token_watch_fd_d gg_token_watch_fd;
gg_typing_notification_d gg_typing_notification;

gg_unregister3_d gg_unregister3;
gg_userlist100_request_d gg_userlist100_request;
gg_userlist_request_d gg_userlist_request;

gg_watch_fd_d gg_watch_fd;

