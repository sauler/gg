﻿/*******************************************************************************
 *                       Copyright (C) 2016 Rafał Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once
#include "libgadu.h"
#include <vcl.h>


/********************************* LIBRARY ********************************** */
bool LoadLibgadu(UnicodeString path);
void FreeLibgadu();

/********************************** SOCKET ********************************** */

typedef int(*gg_socket_manager_connected_d)(void *, void *, int);

/********************************** SESSION ********************************* */

typedef struct gg_session* (*gg_login_d)(const struct gg_login_params *);
typedef int(*gg_is_gpl_compliant_d)(void);
typedef void(*gg_free_session_d)(struct gg_session*);
typedef void(*gg_logoff_d)(struct gg_session*);
typedef int(*gg_change_status_d)(struct gg_session*, int);
typedef int(*gg_change_status_descr_d)(struct gg_session*, int, const char *);
typedef int(*gg_change_status_descr_time_d)(struct gg_session*, int,
	const char *, int);
typedef int(*gg_change_status_flags_d)(struct gg_session*, int);
typedef int(*gg_send_message_d)(struct gg_session*, int, uin_t,
	const unsigned char *);
typedef int(*gg_send_message_richtext_d)(struct gg_session*, int, uin_t,
	const unsigned char *, const unsigned char *, int);
typedef int(*gg_send_message_html_d)(struct gg_session*, int, uin_t,
	const unsigned char *);
typedef int(*gg_send_message_confer_d)(struct gg_session*, int, int, uin_t*,
	const unsigned char *);
typedef int(*gg_send_message_confer_richtext_d)(struct gg_session*, int, int,
	uin_t*, const unsigned char *, const unsigned char *, int);
typedef int(*gg_send_message_confer_html_d)(struct gg_session*, int, int,
	uin_t*, const unsigned char *);
typedef int(*gg_send_message_ctcp_d)(struct gg_session*, int, uin_t,
	const unsigned char *, int);
typedef int(*gg_ping_d)(struct gg_session*);
typedef int(*gg_userlist_request_d)(struct gg_session*, char, const char *);
typedef int(*gg_userlist100_request_d)(struct gg_session*, char, unsigned int,
	char, const char *);
typedef int(*gg_image_request_d)(struct gg_session*, uin_t, int, uint32_t);
typedef int(*gg_image_reply_d)(struct gg_session*, uin_t, const char *,
	const char *, int);
typedef int(*gg_typing_notification_d)(struct gg_session*, uin_t, int);

typedef uint32_t(*gg_crc32_d)(uint32_t, const unsigned char *, int);

typedef int(*gg_session_set_resolver_d)(struct gg_session*, gg_resolver_t);
typedef gg_resolver_t(*gg_session_get_resolver_d)(struct gg_session *);
typedef int(*gg_session_set_custom_resolver_d)(struct gg_session*,
	int(*resolver_start)(int*, void**, const char*),
	void(*resolver_cleanup)(void**, int));

typedef int(*gg_http_set_resolver_d)(struct gg_http*, gg_resolver_t);
typedef gg_resolver_t(*gg_http_get_resolver_d)(struct gg_http *);
typedef int(*gg_http_set_custom_resolver_d)(struct gg_http*,
	int(*resolver_start)(int*, void**, const char*),
	void(*resolver_cleanup)(void**, int));

typedef int(*gg_global_set_resolver_d)(gg_resolver_t);
typedef gg_resolver_t(*gg_global_get_resolver_d)(void);
typedef int(*gg_global_set_custom_resolver_d)(int(*resolver_start)(int*, void**,
	const char*), void(*resolver_cleanup)(void**, int));

/******************************** MULTILOGON ********************************* */

typedef int(*gg_multilogon_disconnect_d)(struct gg_session*,
	gg_multilogon_id_t);

/************************************ CHAT *********************************** */

typedef int(*gg_chat_create_d)(struct gg_session*);
typedef int(*gg_chat_invite_d)(struct gg_session*, uint64_t, uin_t*,
	unsigned int);
typedef int(*gg_chat_leave_d)(struct gg_session * gs, uint64_t id);
typedef int(*gg_chat_send_message_d)(struct gg_session*, uint64_t,
	const char *, int);

/********************************** EVENTS *********************************** */

typedef struct gg_event* (*gg_watch_fd_d)(struct gg_session *);
typedef void(*gg_event_free_d)(struct gg_event*);
typedef int(*gg_notify_ex_d)(struct gg_session*, uin_t*, char *, int);
typedef int(*gg_notify_d)(struct gg_session*, uin_t*, int);
typedef int(*gg_add_notify_ex_d)(struct gg_session*, uin_t, char);
typedef int(*gg_add_notify_d)(struct gg_session*, uin_t);
typedef int(*gg_remove_notify_ex_d)(struct gg_session*, uin_t, char);
typedef int(*gg_remove_notify_d)(struct gg_session*, uin_t);

/********************************** HTTP ************************************* */

typedef struct gg_http* (*gg_http_connect_d)(const char *, int, int,
	const char *, const char *, const char *);
typedef int(*gg_http_watch_fd_d)(struct gg_http*);
typedef void(*gg_http_stop_d)(struct gg_http*);
typedef void(*gg_http_free_d)(struct gg_http*);

/********************************** PUBDIR *********************************** */

typedef uint32_t(*gg_pubdir50_d)(struct gg_session *, gg_pubdir50_t);
typedef gg_pubdir50_t(*gg_pubdir50_new_d)(int type);
typedef int(*gg_pubdir50_add_d)(gg_pubdir50_t, const char *, const char *);
typedef int(*gg_pubdir50_seq_set_d)(gg_pubdir50_t, uint32_t);
typedef const char* (*gg_pubdir50_get_d)(gg_pubdir50_t, int, const char *);
typedef int(*gg_pubdir50_type_d)(gg_pubdir50_t);
typedef int(*gg_pubdir50_count_d)(gg_pubdir50_t);
typedef uin_t(*gg_pubdir50_next_d)(gg_pubdir50_t);
typedef uint32_t(*gg_pubdir50_seq_d)(gg_pubdir50_t);
typedef void(*gg_pubdir50_free_d)(gg_pubdir50_t);

/********************************** TOKEN ************************************ */

typedef struct gg_http* (*gg_token_d)(int);
typedef int(*gg_token_watch_fd_d)(struct gg_http*);
typedef void(*gg_token_free_d)(struct gg_http*);

/**************************** ACCOUNT OPERATIONS ***************************** */

typedef struct gg_http* (*gg_register3_d)(const char *, const char *,
	const char *, const char *, int);
typedef struct gg_http* (*gg_unregister3_d)(uin_t, const char *, const char *,
	const char *, int);
typedef struct gg_http* (*gg_remind_passwd3_d)(uin_t, const char *,
	const char *, const char *, int);
typedef struct gg_http* (*gg_change_passwd4_d)(uin_t, const char *,
	const char *, const char *, const char *, const char *, int);

/*********************************** DCC ************************************* */

typedef int(*gg_dcc_request_d)(struct gg_session*, uin_t);
typedef struct gg_dcc* (*gg_dcc_send_file_d)(uint32_t, uint16_t, uin_t, uin_t);
typedef struct gg_dcc* (*gg_dcc_get_file_d)(uint32_t, uint16_t, uin_t, uin_t);
typedef struct gg_dcc* (*gg_dcc_voice_chat_d)(uint32_t, uint16_t, uin_t, uin_t);
typedef void(*gg_dcc_set_type_d)(struct gg_dcc*, int);
typedef int(*gg_dcc_fill_file_info_d)(struct gg_dcc*, const char *);
typedef int(*gg_dcc_fill_file_info2_d)(struct gg_dcc*, const char *,
	const char *);
typedef int(*gg_dcc_voice_send_d)(struct gg_dcc*, char *, int);
typedef struct gg_dcc* (*gg_dcc_socket_create_d)(uin_t, uint16_t);
typedef struct gg_event* (*gg_dcc_watch_fd_d)(struct gg_dcc *);
typedef void(*gg_dcc_free_d)(struct gg_dcc*);
typedef struct gg_event* (*gg_dcc7_watch_fd_d)(struct gg_dcc7 *);
typedef struct gg_dcc7* (*gg_dcc7_send_file_d)(struct gg_session *, uin_t,
	const char *, const char *, const char *);
typedef struct gg_dcc7* (*gg_dcc7_send_file_fd_d)(struct gg_session *, uin_t,
	int, size_t, const char *, const char *);
typedef int(*gg_dcc7_accept_d)(struct gg_dcc7*, unsigned int);
typedef int(*gg_dcc7_reject_d)(struct gg_dcc7*, int);
typedef void(*gg_dcc7_free_d)(struct gg_dcc7*);

/********************************* DEBUG ************************************* */

typedef const char* (*gg_debug_state_d)(enum gg_state_t);
typedef const char* (*gg_debug_event_d)(enum gg_event_t);
typedef const char* (*gg_libgadu_version_d)(void);
typedef int (*gg_libgadu_check_feature_d)(gg_libgadu_feature_t);


/***************************** DECLARATION ********************************** */

// LIBGADU HANDLE
extern HINSTANCE handle;

// FUNCTIONS

extern gg_add_notify_d gg_add_notify;
extern gg_add_notify_ex_d gg_add_notify_ex;

extern gg_change_passwd4_d gg_change_passwd4;
extern gg_change_status_d gg_change_status;
extern gg_change_status_descr_d gg_change_status_descr;
extern gg_change_status_descr_time_d gg_change_status_descr_time;
extern gg_change_status_flags_d gg_change_status_flags;
extern gg_chat_create_d gg_chat_create;
extern gg_chat_invite_d gg_chat_invite;
extern gg_chat_leave_d gg_chat_leave;
extern gg_chat_send_message_d gg_chat_send_message;
extern gg_crc32_d gg_crc32;

extern gg_dcc7_accept_d gg_dcc7_accept;
extern gg_dcc7_free_d gg_dcc7_free;
extern gg_dcc7_reject_d gg_dcc7_reject;
extern gg_dcc7_send_file_d gg_dcc7_send_file;
extern gg_dcc7_send_file_fd_d gg_dcc7_send_file_fd;
extern gg_dcc7_watch_fd_d gg_dcc7_watch_fd;
extern gg_dcc_fill_file_info_d gg_dcc_fill_file_info;
extern gg_dcc_fill_file_info2_d gg_dcc_fill_file_info2;
extern gg_dcc_free_d gg_dcc_free;
extern gg_dcc_get_file_d gg_dcc_get_file;
extern gg_dcc_request_d gg_dcc_request;
extern gg_dcc_send_file_d gg_dcc_send_file;
extern gg_dcc_set_type_d gg_dcc_set_type;
extern gg_dcc_socket_create_d gg_dcc_socket_create;
extern gg_dcc_voice_chat_d gg_dcc_voice_chat;
extern gg_dcc_voice_send_d gg_dcc_voice_send;
extern gg_dcc_watch_fd_d gg_dcc_watch_fd;

extern gg_event_free_d gg_event_free;

extern gg_free_session_d gg_free_session;

extern gg_global_get_resolver_d gg_global_get_resolver;
extern gg_global_set_custom_resolver_d gg_global_set_custom_resolver;
extern gg_global_set_resolver_d gg_global_set_resolver;

extern gg_http_connect_d gg_http_connect;
extern gg_http_free_d gg_http_free;
extern gg_http_get_resolver_d gg_http_get_resolver;
extern gg_http_set_custom_resolver_d gg_http_set_custom_resolver;
extern gg_http_set_resolver_d gg_http_set_resolver;
extern gg_http_stop_d gg_http_stop;
extern gg_http_watch_fd_d gg_http_watch_fd;

extern gg_image_reply_d gg_image_reply;
extern gg_image_request_d gg_image_request;
extern gg_is_gpl_compliant_d gg_is_gpl_compliant;

extern gg_login_d gg_login;
extern gg_logoff_d gg_logoff;

extern gg_multilogon_disconnect_d gg_multilogon_disconnect;

extern gg_notify_d gg_notify;
extern gg_notify_ex_d gg_notify_ex;

extern gg_ping_d gg_ping;
extern gg_pubdir50_d gg_pubdir50;
extern gg_pubdir50_add_d gg_pubdir50_add;
extern gg_pubdir50_count_d gg_pubdir50_count;
extern gg_pubdir50_free_d gg_pubdir50_free;
extern gg_pubdir50_get_d gg_pubdir50_get;
extern gg_pubdir50_new_d gg_pubdir50_new;
extern gg_pubdir50_next_d gg_pubdir50_next;
extern gg_pubdir50_seq_d gg_pubdir50_seq;
extern gg_pubdir50_seq_set_d gg_pubdir50_seq_set;
extern gg_pubdir50_type_d gg_pubdir50_type;

extern gg_register3_d gg_register3;
extern gg_remind_passwd3_d gg_remind_passwd3;
extern gg_remove_notify_d gg_remove_notify;
extern gg_remove_notify_ex_d gg_remove_notify_ex;

extern gg_send_message_d gg_send_message;
extern gg_send_message_confer_d gg_send_message_confer;
extern gg_send_message_confer_html_d gg_send_message_confer_html;
extern gg_send_message_confer_richtext_d gg_send_message_confer_richtext;
extern gg_send_message_ctcp_d gg_send_message_ctcp;
extern gg_send_message_html_d gg_send_message_html;
extern gg_send_message_richtext_d gg_send_message_richtext;
extern gg_session_get_resolver_d gg_session_get_resolver;
extern gg_session_set_custom_resolver_d gg_session_set_custom_resolver;
extern gg_session_set_resolver_d gg_session_set_resolver;

extern gg_token_d gg_token;
extern gg_token_free_d gg_token_free;
extern gg_token_watch_fd_d gg_token_watch_fd;
extern gg_typing_notification_d gg_typing_notification;

extern gg_unregister3_d gg_unregister3;
extern gg_userlist100_request_d gg_userlist100_request;
extern gg_userlist_request_d gg_userlist_request;

extern gg_watch_fd_d gg_watch_fd;




