/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <System.Classes.hpp>

// Libgadu includes
#include "lib\libgadu\libgadu.h"

class GGSession
{
private:
	gg_session* SessionV;
	struct gg_event* EventV;
	bool Connected;
public:
	GGSession();
	~GGSession();

	gg_session* Session();
	bool Exists();
	void Free();
	bool Login(struct gg_login_params* LoginParams);
	bool IsConnected();
	void IsConnected(bool Connected);
	void LogOff();

	struct gg_event* Event();
	bool EventExists();
	bool WatchFor();
	void FreeEvent();
};
