/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// SDK includes
#include <SDK\AccountManager.h>
#include <SDK\ContactManager.h>
#include <SDK\Settings.h>

// Plugin includes
#include "GGProtocol.h"

class GGPlugin
{
private:
	static GGPlugin* Instance;
	CAccountManager* AccountManager;
	CContactManager* ContactManager;
	CSettings* AccountsSettings;
	GGProtocol* Protocol;

	GGPlugin();
	void ExtractResources();
	void LoadIcons();
	void LoadSettings();
public:
	~GGPlugin();
	static GGPlugin* instance();
	static void InitInstance();
	static void ResetInstance();

	CAccountManager* GetAccountManager();
	CContactManager* GetContactManager();
	GGProtocol* GetProtocol();
};
