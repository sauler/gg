/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include "sSkinManager.hpp"
#include "sSkinProvider.hpp"
#include "sButton.hpp"
#include "sEdit.hpp"
#include "sLabel.hpp"

// Defines
#define WM_ALPHAWINDOWS (WM_USER + 666)

class TAddAccountForm : public TForm
{
__published:
	TsSkinManager *SkinManager;
	TsSkinProvider *SkinProvider;
	TsLabel *LoginLabel;
	TsLabel *UINLabel;
	TsEdit *UINEdit;
	TsLabel *PasswordLabel;
	TsEdit *PasswordEdit;
	TsButton *LoginButton;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall CancelButtonClick(TObject *Sender);
	void __fastcall LoginButtonClick(TObject *Sender);
	void __fastcall FormClose(TObject *Sender, TCloseAction &Action);
private:
	static TAddAccountForm* Instance;
	static INT_PTR __stdcall ThemeChanged(WPARAM wParam, LPARAM lParam);

	void __fastcall UpdateSkin();
	void __fastcall UpdateColors();

	void __fastcall WMTransparency(TMessage &Message);
	BEGIN_MESSAGE_MAP
	MESSAGE_HANDLER(WM_ALPHAWINDOWS,TMessage,WMTransparency);
	END_MESSAGE_MAP(TForm)
public:
	__fastcall TAddAccountForm(TComponent* Owner);
	__fastcall ~TAddAccountForm();
};

extern PACKAGE TAddAccountForm *AddAccountForm;

