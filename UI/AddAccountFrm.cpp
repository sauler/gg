/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "AddAccountFrm.h"

// VCL includes
#include <vcl.h>

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\Paths.h>
#include <SDK\PluginLink.h>
#include <SDK\Theme.h>

// Plugin includes
#include "GGAccount.h"
#include "GGPlugin.h"

#pragma link "sSkinManager"
#pragma link "sSkinProvider"
#pragma link "sButton"
#pragma link "sEdit"
#pragma link "sLabel"
#pragma resource "*.dfm"

TAddAccountForm *AddAccountForm;

TAddAccountForm* TAddAccountForm::Instance = 0;

__fastcall TAddAccountForm::TAddAccountForm(TComponent* Owner)
	: TForm(Owner)
{
	Instance = this;
	CPluginLink::instance()->GetLink().HookEvent(AQQ_SYSTEM_THEMECHANGED,
		ThemeChanged);
}

__fastcall TAddAccountForm::~TAddAccountForm()
{
	CPluginLink::instance()->GetLink().UnhookEvent(ThemeChanged);
}

void __fastcall TAddAccountForm::WMTransparency(TMessage &Message)
{
	Application->ProcessMessages();
	if (SkinManager->Active)
		SkinProvider->BorderForm->UpdateExBordersPos(true,(int)Message.LParam);
}

INT_PTR __stdcall TAddAccountForm::ThemeChanged(WPARAM wParam,LPARAM lParam)
{
	TAddAccountForm::Instance->UpdateSkin();
	return 0;
}

void __fastcall TAddAccountForm::UpdateSkin()
{
	if (CTheme::instance()->SkinEnabled())
	{
		UnicodeString SkinDirectory;
		SkinDirectory = CPaths::instance()->ThemeDir() + "\\Skin";
		if (FileExists(SkinDirectory + "\\Skin.asz"))
		{
			SkinManager->SkinDirectory = SkinDirectory;
			SkinManager->SkinName = "Skin.asz";

			if (CTheme::instance()->AnimateWindows())
				SkinManager->AnimEffects->FormShow->Time = 200;
			else
				SkinManager->AnimEffects->FormShow->Time = 0;

			SkinManager->Effects->AllowGlowing = CTheme::instance()->Glowing();
			UpdateColors();
			SkinManager->Active = true;
		}
		else
			SkinManager->Active = false;
	}
	else
		SkinManager->Active = false;
}

void __fastcall TAddAccountForm::UpdateColors()
{
	SkinManager->HueOffset = CTheme::instance()->Hue();
	SkinManager->Saturation = CTheme::instance()->Saturation();
	SkinManager->Brightness = CTheme::instance()->Brightness();
}
void __fastcall TAddAccountForm::FormCreate(TObject *Sender)
{
	UpdateSkin();
}

void __fastcall TAddAccountForm::LoginButtonClick(TObject *Sender)
{
	if ((UINEdit->Text == "") && (PasswordEdit->Text == ""))
	{
		AQQ::Functions::ShowMessage(1, "Uzupe�nij wszystkie pola!");
		return;
	}

	GGPlugin::instance()->GetAccountManager()->AddAccount(new GGAccount(UINEdit->Text, PasswordEdit->Text));
	GGPlugin::instance()->GetAccountManager()->GetLastAddedAccount()->SaveSettings();
	Close();
}

void __fastcall TAddAccountForm::CancelButtonClick(TObject *Sender)
{
	Close();
}

void __fastcall TAddAccountForm::FormClose(TObject *Sender, TCloseAction &Action)
{
	UINEdit->Text = "";
	PasswordEdit->Text = "";
}

