object AddAccountForm: TAddAccountForm
  Left = 0
  Top = 0
  BorderIcons = [biSystemMenu]
  BorderStyle = bsSingle
  Caption = 'Dodaj konto GG'
  ClientHeight = 182
  ClientWidth = 270
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Icon.Data = {
    0000010001001010000000002000680400001600000028000000100000002000
    000001002000000000004004000000000000000000000000000000000000FFFF
    FF01FFFFFF010808BD17FFFFFF01FFFFFF01FFFFFF01FFFFFF011010EEFF1010
    EFFFFFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFF
    FF010808BD3B0B0BCF75FFFFFF01FFFFFF01FFFFFF01FFFFFF011212FCFF1212
    FDFFFFFFFF01FFFFFF01FFFFFF010F0FEB3F0D0DDF69FFFFFF01FFFFFF01FFFF
    FF010B0BCCFD0D0DDFEB1010EE651111FB0F1212FD6F1212FDCB1212FDFF1212
    FDFF1212FDCB1212FD6F1212FD431111F9F91010EEF5FFFFFF01FFFFFF01FFFF
    FF010C0CD9750F0FEBE91111F9FF1212FDE92071FEFF2CC0FFFF32E4FFFF32E4
    FFFF2CC0FFFF2071FEFF1212FDFF1212FDFF1111F9B1FFFFFF01FFFFFF01FFFF
    FF01FFFFFF011111F5771212FDFF2383FEFF33EDFFFF23ECFFFF05E9FFFF05E9
    FFFF23ECFFFF33EDFFFF2383FEFF1212FDEF1212FD07FFFFFF01FFFFFF01FFFF
    FF01FFFFFF011212FD7B2070FEFF33EDFFFF08EAFFFF00D4E9FF008195FF0081
    95FF00D4E9FF08EAFFFF33EDFFFF2070FEFF1212FD6FFFFFFF01FFFFFF01FFFF
    FF01FFFFFF011212FDC72CBDFFFF23ECFFFF00D4E9FF008195FF00E9FFFF00E9
    FFFF008195FF00D4E9FF23ECFFFF2CBDFFFF1212FDC7FFFFFF01FFFFFF010D0D
    DBFF1010EFFF1212FDFF32E8FFFF04E9FFFF00E9FFFF00E9FFFF00E9FFFF00E9
    FFFF00E9FFFF00E9FFFF06EAFFFF31E3FFFF1212FDFF1212FDFF1010EFFF0C0C
    D9FF1010EEFF1212FDFF32E8FFFF04E9FFFF00E9FFFF00E9FFFF00E9FFFF00E9
    FFFF00E9FFFF00E9FFFF04E9FFFF32E7FFFF1212FDFF1212FCFF1010EEFFFFFF
    FF01FFFFFF011212FDC72CBDFFFF23ECFFFF00CAE0FF00384BFF00E9FFFF00E9
    FFFF00384BFF00CAE0FF23ECFFFF2CBDFFFF1212FDC7FFFFFF01FFFFFF01FFFF
    FF01FFFFFF011212FC7B2070FEFF33EDFFFF08EAFFFF00E9FFFF00E9FFFF00E9
    FFFF00E9FFFF08EAFFFF33EDFFFF2070FEFF1212FD6DFFFFFF01FFFFFF01FFFF
    FF01FFFFFF010F0FEC771212FCFF2383FEFF33EDFFFF23ECFFFF06EAFFFF06EA
    FFFF23ECFFFF33EDFFFF2383FEFF1212FDEF1212FD07FFFFFF01FFFFFF01FFFF
    FF010B0BCC750D0DDFE91010F0FF1212FDE92070FEFF2CBDFFFF31E3FFFF31E3
    FFFF2CBDFFFF2070FEFF1212FDFF1111F9FF1010EEB1FFFFFF01FFFFFF01FFFF
    FF010808BDFD0B0BCFEB0D0DDF651010F30F1212FD6F1212FDC71212FDFF1212
    FDFF1212FDC71212FD6F1111F5430F0FEBF90D0DDFF5FFFFFF01FFFFFF01FFFF
    FF010606AD3B0808BD75FFFFFF01FFFFFF01FFFFFF01FFFFFF011010EEFF1010
    EFFFFFFFFF01FFFFFF01FFFFFF010C0CD93F0B0BCC69FFFFFF01FFFFFF01FFFF
    FF01FFFFFF010505AB17FFFFFF01FFFFFF01FFFFFF01FFFFFF010C0CD9FF0D0D
    DBFFFFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF01FFFFFF010000
    FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000
    FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF0000FFFF}
  OldCreateOrder = False
  Position = poDesktopCenter
  OnClose = FormClose
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object LoginLabel: TsLabel
    Left = 12
    Top = 14
    Width = 76
    Height = 19
    Caption = 'Logowanie'
    ParentFont = False
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = []
  end
  object UINLabel: TsLabel
    Left = 20
    Top = 44
    Width = 106
    Height = 13
    Caption = 'Numer GG lub telefon:'
  end
  object PasswordLabel: TsLabel
    Left = 20
    Top = 96
    Width = 31
    Height = 13
    Caption = 'Has'#322'o:'
  end
  object UINEdit: TsEdit
    Left = 20
    Top = 60
    Width = 235
    Height = 25
    AutoSize = False
    BiDiMode = bdLeftToRight
    NumbersOnly = True
    ParentBiDiMode = False
    TabOrder = 0
  end
  object PasswordEdit: TsEdit
    Left = 20
    Top = 114
    Width = 235
    Height = 25
    AutoSize = False
    PasswordChar = '*'
    TabOrder = 1
  end
  object LoginButton: TsButton
    Left = 20
    Top = 146
    Width = 80
    Height = 28
    Caption = 'Zaloguj si'#281
    TabOrder = 2
    OnClick = LoginButtonClick
  end
  object SkinManager: TsSkinManager
    ExtendedBorders = True
    Active = False
    InternalSkins = <>
    MenuSupport.IcoLineSkin = 'ICOLINE'
    MenuSupport.ExtraLineFont.Charset = DEFAULT_CHARSET
    MenuSupport.ExtraLineFont.Color = clWindowText
    MenuSupport.ExtraLineFont.Height = -11
    MenuSupport.ExtraLineFont.Name = 'Tahoma'
    MenuSupport.ExtraLineFont.Style = []
    SkinDirectory = 'c:\Skins'
    SkinInfo = 'N/A'
    ThirdParty.ThirdEdits = ' '
    ThirdParty.ThirdButtons = 'TButton'
    ThirdParty.ThirdBitBtns = ' '
    ThirdParty.ThirdCheckBoxes = ' '
    ThirdParty.ThirdGroupBoxes = ' '
    ThirdParty.ThirdListViews = ' '
    ThirdParty.ThirdPanels = ' '
    ThirdParty.ThirdGrids = ' '
    ThirdParty.ThirdTreeViews = ' '
    ThirdParty.ThirdComboBoxes = ' '
    ThirdParty.ThirdWWEdits = ' '
    ThirdParty.ThirdVirtualTrees = ' '
    ThirdParty.ThirdGridEh = ' '
    ThirdParty.ThirdPageControl = ' '
    ThirdParty.ThirdTabControl = ' '
    ThirdParty.ThirdToolBar = ' '
    ThirdParty.ThirdStatusBar = ' '
    ThirdParty.ThirdSpeedButton = ' '
    ThirdParty.ThirdScrollControl = ' '
    ThirdParty.ThirdUpDown = ' '
    ThirdParty.ThirdScrollBar = ' '
    ThirdParty.ThirdStaticText = ' '
    ThirdParty.ThirdNativePaint = ' '
    Left = 328
    Top = 24
  end
  object SkinProvider: TsSkinProvider
    AddedTitle.Font.Charset = DEFAULT_CHARSET
    AddedTitle.Font.Color = clNone
    AddedTitle.Font.Height = -11
    AddedTitle.Font.Name = 'Tahoma'
    AddedTitle.Font.Style = []
    FormHeader.AdditionalHeight = 0
    SkinData.SkinSection = 'FORM'
    TitleButtons = <>
    Left = 328
    Top = 96
  end
end
