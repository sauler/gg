/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGStateChangeService.h"

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\IconManager.h>
#include <SDK\PluginLink.h>

GGStateChangeService* GGStateChangeService::Instance = NULL;

GGStateChangeService::GGStateChangeService(GGStateChange* StateChange)
{
	this->StateChange = StateChange;
	this->CreateServices();
}

GGStateChangeService::~GGStateChangeService()
{
	this->DestroyServices();
}

void GGStateChangeService::CreateServices()
{
	Instance = this;

	RegisterService(StateChange->GetPopupServiceName("Online"), this->Online);
	RegisterService(StateChange->GetPopupServiceName("FFC"), this->FFC);
	RegisterService(StateChange->GetPopupServiceName("Away"), this->Away);
	RegisterService(StateChange->GetPopupServiceName("DND"), this->DND);
	RegisterService(StateChange->GetPopupServiceName("Invisible"), this->Invisible);
	RegisterService(StateChange->GetPopupServiceName("Offline"), this->Offline);
	RegisterService(StateChange->GetPopupServiceName("ChangeDesc"), this->ChangeDesc);
}

void GGStateChangeService::DestroyServices()
{
	UnregisterService(this->Online);
	UnregisterService(this->FFC);
	UnregisterService(this->Away);
	UnregisterService(this->DND);
	UnregisterService(this->Invisible);
	UnregisterService(this->Offline);
	UnregisterService(this->ChangeDesc);

	Instance = NULL;
}

HANDLE GGStateChangeService::RegisterService(UnicodeString Name, TAQQService ServiceProc)
{
	return CPluginLink::instance()->GetLink().CreateServiceFunction(Name.c_str(), ServiceProc);
}

INT_PTR GGStateChangeService::UnregisterService(HANDLE hService)
{
	return CPluginLink::instance()->GetLink().DestroyServiceFunction(hService);
}

INT_PTR __stdcall GGStateChangeService::Online(WPARAM wParam, LPARAM lParam)
{
	int OnlineIcon = CIconManager::instance()->GetIconByName("GG_ONLINE");
	GGStateChangeService::Instance->StateChange->StateButton->BlinkStopIcon = OnlineIcon;

	if (GGStateChangeService::Instance->StateChange->GetConnection()->IsConnected())
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->SendStatus(CONTACT_ONLINE, "Online");
		GGStateChangeService::Instance->StateChange->StateButton->IconID = OnlineIcon;
		GGStateChangeService::Instance->StateChange->StateButton->Update();
	}
	else
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->Login();
		GGStateChangeService::Instance->StateChange->StateButton->Blink();
		// TODO: Set status after connect
	}
}

INT_PTR __stdcall GGStateChangeService::FFC(WPARAM wParam, LPARAM lParam)
{
	int FFCIcon = CIconManager::instance()->GetIconByName("GG_FFC");
	GGStateChangeService::Instance->StateChange->StateButton->BlinkStopIcon = FFCIcon;

	if (GGStateChangeService::Instance->StateChange->GetConnection()->IsConnected())
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->SendStatus(CONTACT_FFC, "FFC");
		GGStateChangeService::Instance->StateChange->StateButton->IconID = FFCIcon;
		GGStateChangeService::Instance->StateChange->StateButton->Update();
	}
	else
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->Login();
		GGStateChangeService::Instance->StateChange->StateButton->Blink();
		// TODO: Set status after connect
	}

}

INT_PTR __stdcall GGStateChangeService::Away(WPARAM wParam, LPARAM lParam)
{
	int AwayIcon = CIconManager::instance()->GetIconByName("GG_AWAY");
	GGStateChangeService::Instance->StateChange->StateButton->BlinkStopIcon = AwayIcon;

	if (GGStateChangeService::Instance->StateChange->GetConnection()->IsConnected())
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->SendStatus(CONTACT_AWAY, "Away");
		GGStateChangeService::Instance->StateChange->StateButton->IconID = AwayIcon;
		GGStateChangeService::Instance->StateChange->StateButton->Update();
	}
	else
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->Login();
		GGStateChangeService::Instance->StateChange->StateButton->Blink();
		// TODO: Set status after connect
	}
}

INT_PTR __stdcall GGStateChangeService::DND(WPARAM wParam, LPARAM lParam)
{
	int DNDIcon = CIconManager::instance()->GetIconByName("GG_DND");
	GGStateChangeService::Instance->StateChange->StateButton->BlinkStopIcon = DNDIcon;

	if (GGStateChangeService::Instance->StateChange->GetConnection()->IsConnected())
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->SendStatus(CONTACT_DND, "DND");
		GGStateChangeService::Instance->StateChange->StateButton->IconID = DNDIcon;
		GGStateChangeService::Instance->StateChange->StateButton->Update();
	}
	else
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->Login();
		GGStateChangeService::Instance->StateChange->StateButton->Blink();
		// TODO: Set status after connect
	}
}

INT_PTR __stdcall GGStateChangeService::Invisible(WPARAM wParam, LPARAM lParam)
{
	int InvisibleIcon = CIconManager::instance()->GetIconByName("GG_INVISIBLE");
	GGStateChangeService::Instance->StateChange->StateButton->BlinkStopIcon = InvisibleIcon;

	if (GGStateChangeService::Instance->StateChange->GetConnection()->IsConnected())
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->SendStatus(CONTACT_INV, "Invisible");
		GGStateChangeService::Instance->StateChange->StateButton->IconID = InvisibleIcon;
		GGStateChangeService::Instance->StateChange->StateButton->Update();
	}
	else
	{
		GGStateChangeService::Instance->StateChange->GetConnection()->Login();
		GGStateChangeService::Instance->StateChange->StateButton->Blink();
		// TODO: Set status after connect
	}
}

INT_PTR __stdcall GGStateChangeService::Offline(WPARAM wParam, LPARAM lParam)
{
	int OfflineIcon = CIconManager::instance()->GetIconByName("GG_OFFLINE");
	GGStateChangeService::Instance->StateChange->StateButton->BlinkStopIcon = OfflineIcon;

	GGStateChangeService::Instance->StateChange->StateButton->IconID = OfflineIcon;
	GGStateChangeService::Instance->StateChange->StateButton->Update();
	GGStateChangeService::Instance->StateChange->GetConnection()->SendStatus(CONTACT_OFFLINE, "Offline");
}

INT_PTR __stdcall GGStateChangeService::ChangeDesc(WPARAM wParam, LPARAM lParam)
{
	AQQ::System::RunAction("aNote");
}

