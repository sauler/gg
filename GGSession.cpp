/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGSession.h"

// Libgadu includes
#include "lib\libgadu\libgadu_functions.h"

GGSession::GGSession()
{
	SessionV = 0;
	EventV = 0;
	Connected = false;
}

GGSession::~GGSession()
{

}

gg_session* GGSession::Session()
{
	return SessionV;
}

bool GGSession::Exists()
{
	if (!SessionV)
		return false;
	else
		return true;
}

void GGSession::Free()
{
	gg_free_session(SessionV);
	SessionV = 0;
}

bool GGSession::Login(struct gg_login_params* LoginParams)
{
	SessionV = gg_login(LoginParams);
	if (!SessionV)
		return false;
	else
		return true;
}

bool GGSession::IsConnected()
{
	return this->Connected;
}

void GGSession::IsConnected(bool Connected)
{
	this->Connected = Connected;
}

void GGSession::LogOff()
{
	if (Exists())
		gg_logoff(SessionV);
}

struct gg_event* GGSession::Event()
{
	return EventV;
};

bool GGSession::EventExists()
{
	if (!EventV)
		return false;
	else
		return true;
}

bool GGSession::WatchFor()
{
	EventV = gg_watch_fd(SessionV);

	if (!EventV)
		return false;
	else
		return true;
}

void GGSession::FreeEvent()
{
	gg_event_free(EventV);
	EventV = 0;
}
