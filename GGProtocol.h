/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <vcl.h>

// SDK includes
#include <SDK\Protocol.h>

class GGProtocol: private CProtocol
{
public:
	GGProtocol();
	~GGProtocol();

	virtual int MaxMessageLength();
	virtual int MaxStatusLength();
	virtual bool CheckContact(UnicodeString JID);
	virtual UnicodeString Clipboard(UnicodeString JID);
	virtual UnicodeString OnlineIconPath();
	virtual UnicodeString FFCIconPath();
	virtual UnicodeString AwayIconPath();
	virtual UnicodeString NAIconPath();
	virtual UnicodeString DNDIconPath();
	virtual UnicodeString InvisibleIconPath();
	virtual UnicodeString OfflineIconPath();
	virtual UnicodeString BlockedIconPath();
	virtual int OnlineIcon();
	virtual int FFCIcon();
	virtual int AwayIcon();
	virtual int NAIcon();
	virtual int DNDIcon();
	virtual int InvisibleIcon();
	virtual int OfflineIcon();
	virtual int BlockedIcon();
};


