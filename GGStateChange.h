/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#pragma once

// VCL includes
#include <vcl.h>

// SDK includes
#include <SDK\Popup.h>
#include <SDK\StateButton.h>

// Plugin includes
#include "GGConnection.h"

class GGStateChange
{
private:
	GGConnection* Connection;
	CPopup* StatePopup;
	int AccountID;

	UnicodeString GetPopupName();
	UnicodeString GetPopupItemName(UnicodeString state);
	UnicodeString GetButtonName();
public:
	CStateButton* StateButton;
	GGStateChange(GGConnection* Connection);
	GGStateChange(int AccountID, GGConnection* Connection);
	~GGStateChange();

	void BuildStatePopup();
	void BuildStateButton();

	UnicodeString GetPopupServiceName(UnicodeString state);

    GGConnection* GetConnection();
};

