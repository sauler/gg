/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include <vcl.h>
#include <windows.h>

// SDK includes
#include <SDK\Paths.h>
#include <SDK\PluginAPI.h>
#include <SDK\PluginLink.h>

#include "GGPlugin.h"
//#include "GG\EventHandler.h"

#include "lib\libgadu\libgadu_functions.h"

TPluginInfo PluginInfo;
//CEventHandler *eventHandler;

int WINAPI DllEntryPoint(HINSTANCE hinst,unsigned long reason,void* lpReserved)
{
	return 1;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Load(PPluginLink Link)
{
	CPluginLink::instance()->SetLink(*Link);
	LoadLibgadu(CPaths::instance()->PluginUserDir() + "\\GG\\Libs\\libgadu.dll");
	GGPlugin::InitInstance();
	//eventHandler = new CEventHandler();

	return 0;
}

extern "C" INT_PTR __declspec(dllexport) __stdcall Unload()
{
	GGPlugin::ResetInstance();
	//delete eventHandler;
	FreeLibgadu();
	return 0;
}

extern "C" __declspec(dllexport) PPluginInfo __stdcall AQQPluginInfo
	(DWORD AQQVersion)
{
	PluginInfo.cbSize = sizeof(TPluginInfo);
	PluginInfo.ShortName = L"GG";
	PluginInfo.Version = PLUGIN_MAKE_VERSION(0,1,0,0);
	PluginInfo.Description = L"Obs�uga protoko�u GG.";
	PluginInfo.Author = L"Rafa� Babiarz (sauler)";
	PluginInfo.AuthorMail = L"sauler1995@gmail.com";
	PluginInfo.Copyright = L"sauler";
	PluginInfo.Homepage = L"";
	PluginInfo.Flag = 0;
	PluginInfo.ReplaceDefaultModule = 0;

	return &PluginInfo;
}

