/*******************************************************************************
 *                       Copyright (C) 2016 Rafa� Babiarz                      *
 *                                                                             *
 * This file is part of GG plugin                                              *
 *                                                                             *
 * GG plugin is free software: you can redistribute it and/or modify           *
 * it under the terms of the GNU General Public License as published by        *
 * the Free Software Foundation; either version 3, or (at your option)         *
 * any later version.                                                          *
 *                                                                             *
 * GG is distributed in the hope that it will be useful,                       *
 * but WITHOUT ANY WARRANTY; without even the implied warranty of              *
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the                *
 * GNU General Public License for more details.                                *
 *                                                                             *
 * You should have received a copy of the GNU General Public License           *
 * along with GNU Radio. If not, see <http://www.gnu.org/licenses/>.           *
 ******************************************************************************/

#include "GGProtocol.h"

// SDK includes
#include <SDK\AQQ.h>
#include <SDK\IconManager.h>

// Libgadu includes
#include "lib\libgadu\libgadu.h"

// Other includes
#include <boost/regex.hpp>

GGProtocol::GGProtocol()
{
	this->JID = "gg.contact";
	this->Name = "Kontakt GG";
	this->Prompt = "Numer GG";
	this->Transport = true;
	this->Search = true;
	this->GroupChat = true;
	this->Description = "Wtyczka obs�uguj�ca sie� GG";
	this->RequiredID = true;
	this->IconID = CIconManager::instance()->GetIconByName("GG_ONLINE");
	this->AccountName = "Konto GG";
	this->Register();
}


GGProtocol::~GGProtocol()
{
	this->Remove();
}

int GGProtocol::MaxMessageLength()
{
	return GG_MSG_MAXSIZE;
}

int GGProtocol::MaxStatusLength()
{
    return GG_STATUS_DESCR_MAXSIZE;
}

bool GGProtocol::CheckContact(UnicodeString JID)
{
	boost::wregex regex(L"^([0-9])+@gg\.contact");
	if (boost::regex_match(JID.w_str(), regex))
		return true;
	else
		return false;
}

UnicodeString GGProtocol::Clipboard(UnicodeString JID)
{
	if (CheckContact(JID))
		return StringReplace(JID, "@gg.contact", "",
			TReplaceFlags() << rfReplaceAll);
	else
		return JID;
}

UnicodeString GGProtocol::OnlineIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_ONLINE");
}

UnicodeString GGProtocol::FFCIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_FFC");
}

UnicodeString GGProtocol::AwayIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_AWAY");
}

UnicodeString GGProtocol::NAIconPath()
{
	return AwayIconPath();
}

UnicodeString GGProtocol::DNDIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_DND");
}

UnicodeString GGProtocol::InvisibleIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_INVISIBLE");
}

UnicodeString GGProtocol::OfflineIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_OFFLINE");
}

UnicodeString GGProtocol::BlockedIconPath()
{
	return CIconManager::instance()->GetIconPathByName("GG_BLOCKED");
}

int GGProtocol::OnlineIcon()
{
	return CIconManager::instance()->GetIconByName("GG_ONLINE");
}

int GGProtocol::FFCIcon()
{
	return CIconManager::instance()->GetIconByName("GG_FFC");
}

int GGProtocol::AwayIcon()
{
	return CIconManager::instance()->GetIconByName("GG_AWAY");
}

int GGProtocol::NAIcon()
{
	return AwayIcon();
}

int GGProtocol::DNDIcon()
{
	return CIconManager::instance()->GetIconByName("GG_DND");
}

int GGProtocol::InvisibleIcon()
{
	return CIconManager::instance()->GetIconByName("GG_INVISIBLE");
}

int GGProtocol::OfflineIcon()
{
	return CIconManager::instance()->GetIconByName("GG_OFFLINE");
}

int GGProtocol::BlockedIcon()
{
	return CIconManager::instance()->GetIconByName("GG_BLOCKED");
}

